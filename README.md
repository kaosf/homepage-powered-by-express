# My Homepage Powered by Express [![Build Status](https://travis-ci.org/kaosf/homepage-powered-by-express.png)](https://travis-ci.org/kaosf/homepage-powered-by-express)

[kaosfield.net](http://kaosfield.net)

[on Heroku](http://ka-express.herokuapp.com)

## References

* [Jade - Template Engine](http://jade-lang.com/)
* [kaosf/jade-h5bp](https://github.com/kaosf/jade-h5bp)
* [kaosf/nodejs-express-markdown](https://github.com/kaosf/nodejs-express-markdown)
